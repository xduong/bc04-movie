import React, { useEffect, useState } from "react";
import { moviesServ } from "../../services/movieService";
import ItemMovie from "./ItemMovie";
import TabMovies from "./TabMovies";
import Spinner from "../../Components/spinner/Spinner";
import { useDispatch } from "react-redux";
import {
  setLoadingOffAction,
  setLoadingOnAction,
} from "../../redux/actions/actionSpinner";

export default function HomePage() {
  const [movies, setMovies] = useState([]);
  // const [isLoading, setIsLoading] = useState([]);

  let dispatch = useDispatch();

  useEffect(() => {
    // setIsLoading(true);
    dispatch(setLoadingOnAction());
    moviesServ
      .getListMovie()
      .then((res) => {
        setMovies(res.data.content);
        console.log(res);
        // setIsLoading(false);
        dispatch(setLoadingOffAction());
      })
      .catch((err) => {
        console.log(err);
        // setIsLoading(false);
        dispatch(setLoadingOffAction());
      });
  }, []);
  const renderMovies = () => {
    return movies.map((data, index) => {
      return <ItemMovie key={index} data={data} />;
    });
  };
  return (
    <div className="container mx-auto ">
      <div className="grid grid-cols-4 gap-4 py-10">{renderMovies()}</div>
      <TabMovies />
      {/* {isLoading && <Spinner />} */}
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
}
