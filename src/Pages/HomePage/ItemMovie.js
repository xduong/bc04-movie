import { Card } from "antd";
import React from "react";
import { NavLink } from "react-router-dom";

const { Meta } = Card;

export default function ItemMovie({ data }) {
  return (
    <Card
      hoverable
      style={{ width: "100%" }}
      cover={<img className="h-80 w-full object-cover" src={data.hinhAnh} />}
    >
      <Meta title={<p className="text-red-500 truncate">{data.tenPhim}</p>} />
      <NavLink to={`/detail/${data.maPhim}`}>
        <button className="w-full py-2 text-center bg-red-500 text-white rounded transition cursor-pointer hover:bg-black">
          Xem chi tiết
        </button>
      </NavLink>
    </Card>
  );
}
