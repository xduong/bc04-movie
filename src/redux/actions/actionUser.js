import { message } from "antd";
import { localService } from "../../services/localService";
import { userService } from "../../services/userService";
import { SET_USER } from "../constants/constantUser";

const setUserLoginSuccess = (successValue) => {
  return { type: SET_USER, payload: successValue };
};

//thunk

export const setUserLoginActionServ = (
  dataLogin,
  onLoginSuccess,
  onLoginFail
) => {
  return (dispatch) => {
    userService
      .postLogin(dataLogin)
      .then((res) => {
        localService.user.set(res.data.content);
        onLoginSuccess();
        dispatch(setUserLoginSuccess(res.data.content));
      })
      .catch((err) => {
        onLoginFail();
        console.log(err);
      });
  };
};
