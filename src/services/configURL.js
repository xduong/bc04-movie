import axios from "axios";

export const BASE_URL = "https://movienew.cybersoft.edu.vn";

export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNCIsIkhldEhhblN0cmluZyI6IjIwLzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3Njg1MTIwMDAwMCIsIm5iZiI6MTY1NDEwMjgwMCwiZXhwIjoxNjc2OTk4ODAwfQ.QYLXMgjth5hQh9opZbNS7JEDPZGWA3o_95kR_VyLix8";

export let https = axios.create({
  baseURL: BASE_URL,
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
  },
});
// {
//     "maPhim": 1333,
//     "tenPhim": "Trainwreckk",
//     "biDanh": "trainwreckk",
//     "trailer": "https://www.youtube.com/embed/2MxnhBPoIx4",
//     "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/trainwreckk_gp05.jpg",
//     "moTa": "<p>Having thought that monogamy was never possible, a commitment-phobic career woman may have to face her fears when she meets a good guy.</p>",
//     "maNhom": "GP05",
//     "ngayKhoiChieu": "2021-01-11T00:00:00",
//     "danhGia": 10,
//     "hot": false,
//     "dangChieu": true,
//     "sapChieu": false
// }
